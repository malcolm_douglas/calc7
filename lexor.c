/* The lexical analyzer returns a double floating point
number on the stack and the token NUM, or the numeric code
of the  character read if not a number.  It skips all blanks
and tabs, and returns 0 for end-of-input.  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "calc.h"
#include "parser.h"
 
extern symtable main_symtable;
extern FILE *input_file;

struct keyword_entry {
    char *word;
    int token;
}; 

struct keyword_entry keywords[] =
    { {"if", IF},
      {"else", ELSE},
      {"call", RUN},
      {"jump", JUMP},
      {"while", WHILE},
    };

#define KEYWORDS_LENGTH (sizeof(keywords) / sizeof(struct keyword_entry))

int yylex (void)
{
    int c;

    /* Skip white space.  */
    while ((c = getc (input_file)) == ' ' || c == '\t')
	continue;
    /* Process numbers.  */
    if (c == '.' || isdigit (c)) {
	ungetc (c, input_file);
        fscanf (input_file,"%lf", &yylval.val);
        return VAL;
    }
/* convert strings to symbols */
            
/* Char starts an identifier => read the name.       */
    if (isalpha (c)) {
/* Initially make the buffer long enough
   for a 40-character symbol name.  */
	static size_t length = 40;
	static char *symbuf = 0;
	symbol s;
	unsigned int i;
	if (!symbuf)
	    symbuf = (char *) malloc (length + 1);
     
	i = 0;
	do {
	/* If buffer is full, make it bigger.        */
	    if (i == length) {
		length *= 2;
		symbuf = (char *) realloc (symbuf, length + 1);
	    }
	    /* Add this character to the buffer.         */
	    symbuf[i++] = c;
	    /* Get another character.                    */
	    c = getc(input_file);
	} while (isalnum (c) || c == '_');
     
	ungetc (c, input_file);
	symbuf[i] = '\0';

	for ( i = 0;( i < KEYWORDS_LENGTH); i++) {
	    if(strcmp(symbuf, keywords[i].word) == 0)
		return keywords[i].token;
	}

	s = get_symbol_from_string(symbuf, &main_symtable);
	yylval.sym = s;
	return SYM;
	   
    }

    /* Return end-of-input.  */
    if (c == EOF)
	return 0;
    /* Return a single char.  */
    return c;
}

/* Called by yyparse on error.  */

void
yyerror (char const *s)
{
fprintf (stderr, "%s\n", s);
}
