struct value;
typedef struct symtable {
    char ** table;
    int length;
    struct value * valtable;
} symtable;
//this might be overly complicated, but symbols
//should be treated as opaque anyway.  
typedef struct symbol {
    int id;
    symtable *st;
} symbol;
enum {V_NUM, V_FUNC};
typedef struct value {
    int type;
    union {
	double      x;
	void (*func) (void);
    };
} value ;
typedef value (*built_in) (symtable *);

void declare_builtins (symtable *st);
void install_builtins (symtable *st);

