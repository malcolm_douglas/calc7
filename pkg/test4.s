	.data
	.globl _arg1
	.align 4
_arg1:
	.long 6
	.globl _mod
	.align 4
_mod:
	.long 13
	.globl _mod_part
	.align 4
_mod_part:
	.long 14
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 22
	.globl _print_num
	.align 4
_print_num:
	.long 19

	.comm _main_valtable, 368

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x0
	pushl $0x0
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x0
	pushl $0x0
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x0
	pushl $0x0
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x0
	pushl $0x0
	movl $48, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x3ff00000
	pushl $0x0
	movl $64, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $80, %eax
	addl $_main_valtable, %eax
	movl $_not_equal_0, %ebx
	movl %ebx, 8(%eax)
	movl $112, %eax
	addl $_main_valtable, %eax
	movl $_equal_1, %ebx
	movl %ebx, 8(%eax)
	movl $128, %eax
	addl $_main_valtable, %eax
	movl $_or_2, %ebx
	movl %ebx, 8(%eax)
	movl $144, %eax
	addl $_main_valtable, %eax
	movl $_and_3, %ebx
	movl %ebx, 8(%eax)
	movl $160, %eax
	addl $_main_valtable, %eax
	movl $_is_factor_4, %ebx
	movl %ebx, 8(%eax)
	movl $240, %eax
	addl $_main_valtable, %eax
	movl $_is_prime_5, %ebx
	movl %ebx, 8(%eax)
	pushl $0x40140000
	pushl $0x0
	movl $96, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $304, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	pushl $0x40f86a00
	pushl $0x0
	movl $320, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x3ff00000
	pushl $0x0
	movl $336, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
L17:
	movl $288, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L18
	pushl %ebp
	movl %esp, %ebp
	movl $336, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0x3ff00000
	pushl $0x0
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	addsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $336, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $320, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $336, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $96, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $112, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L19
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L20
L19:
	pushl %ebp
	movl %esp, %ebp
	movl $240, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L21
	pushl %ebp
	movl %esp, %ebp
	movl $304, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	leave
L21:
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L20:
	leave
jmp L17
L18:
	movl $0, %eax
	leave
	ret
_is_prime_5:
	pushl %ebp
	movl %esp, %ebp
	movl $96, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $256, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x40000000
	pushl $0x0
	movl $272, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
L12:
	movl $288, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L13
	pushl %ebp
	movl %esp, %ebp
	movl $272, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $96, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $256, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $112, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L14
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L15
L14:
	pushl %ebp
	movl %esp, %ebp
	movl $272, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $96, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $256, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $160, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L16
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $288, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L16:
	leave
L15:
	movl $272, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0x3ff00000
	pushl $0x0
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	addsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $272, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
jmp L12
L13:
	leave
	ret
_is_factor_4:
	pushl %ebp
	movl %esp, %ebp
	movl $96, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $176, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $192, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $192, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $176, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	divsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $96, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $208, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $224, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L10
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L11
L10:
	pushl %ebp
	movl %esp, %ebp
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L11:
	leave
	ret
_and_3:
	pushl %ebp
	movl %esp, %ebp
	movl $96, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L6
	pushl %ebp
	movl %esp, %ebp
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L8
	pushl %ebp
	movl %esp, %ebp
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L9
L8:
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L9:
	leave
	jmp L7
L6:
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L7:
	leave
	ret
_or_2:
	pushl %ebp
	movl %esp, %ebp
	movl $96, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L2
	pushl %ebp
	movl %esp, %ebp
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L3
L2:
	pushl %ebp
	movl %esp, %ebp
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L4
	pushl %ebp
	movl %esp, %ebp
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L5
L4:
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L5:
	leave
L3:
	leave
	ret
_equal_1:
	pushl %ebp
	movl %esp, %ebp
	movl $80, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L0
	pushl %ebp
	movl %esp, %ebp
	movl $48, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	jmp L1
L0:
	pushl %ebp
	movl %esp, %ebp
	movl $64, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
L1:
	leave
	ret
_not_equal_0:
	pushl %ebp
	movl %esp, %ebp
	movl $96, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	subsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	ret
