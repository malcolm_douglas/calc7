	.data
	.globl _arg1
	.align 4
_arg1:
	.long 2
	.globl _mod
	.align 4
_mod:
	.long 4
	.globl _mod_part
	.align 4
_mod_part:
	.long 5
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 6
	.globl _print_num
	.align 4
_print_num:
	.long 3

	.comm _main_valtable, 112

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x40140000
	pushl $0x0
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	pushl $0x40000000
	pushl $0x0
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
L0:
	movl $0, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0
	pushl $0
	movsd (%esp), %xmm0
	addl $8, %esp
	movsd (%esp), %xmm1
	ucomisd %xmm0, %xmm1
	je L1
	pushl %ebp
	movl %esp, %ebp
	movl $0, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	pushl $0x3ff00000
	pushl $0x0
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	subsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movsd 8(%esp), %xmm0
	movsd (%esp), %xmm1
	addsd %xmm1, %xmm0
	addl $8, %esp
	movsd %xmm0, (%esp)
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
jmp L0
L1:
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 12(%eax)
	pushl 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $48, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $0, %eax
	leave
	ret
