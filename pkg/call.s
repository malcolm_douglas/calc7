	.data
	.globl _arg1
	.align 4
_arg1:
	.long 0
	.globl _mod
	.align 4
_mod:
	.long 2
	.globl _mod_part
	.align 4
_mod_part:
	.long 3
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 4
	.globl _print_num
	.align 4
_print_num:
	.long 1

	.comm _main_valtable, 80

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x40140000
	pushl $0x0
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	movl $16, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $0, %eax
	leave
	ret
