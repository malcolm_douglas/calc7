#include <stdio.h>
#include <math.h>
#include <time.h>
#include "calc_std.h"

extern int arg1;
extern int print_num;
extern int mod_part;
extern int mod_whole;
extern int mod;

extern struct value main_valtable[];

void print_num0 (void);
void mod0 (void);

void program1(void);

int main(void) 
{
    clock_t c = clock();
    printf("Beginning program....\n");

    main_valtable[print_num].type = V_FUNC;
    main_valtable[print_num].func = print_num0;
    main_valtable[mod].type = V_FUNC;
    main_valtable[mod].func = mod0;

    program1();

    printf("took me %f seconds.\n",(float) (clock()-c)/ (float)(CLOCKS_PER_SEC));

    return 0;
}

void print_num0 (void)
{
    printf("%f\n", main_valtable[arg1].x);
}

void mod0 (void)
{
    double whole, part;
    part = modf(main_valtable[arg1].x, &whole);
    main_valtable[mod_whole].type = V_NUM;
    main_valtable[mod_whole].x = whole;
    main_valtable[mod_part].type = V_NUM;
    main_valtable[mod_part].x = part;    
}

