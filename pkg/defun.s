	.data
	.globl _arg1
	.align 4
_arg1:
	.long 1
	.globl _mod
	.align 4
_mod:
	.long 3
	.globl _mod_part
	.align 4
_mod_part:
	.long 4
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 5
	.globl _print_num
	.align 4
_print_num:
	.long 2

	.comm _main_valtable, 96

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	movl $0, %eax
	addl $_main_valtable, %eax
	movl $_set_arg_0, %ebx
	movl %ebx, 8(%eax)
	movl $0, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $0, %eax
	leave
	ret
_set_arg_0:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x40240000
	pushl $0x0
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 8(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 12(%eax)
	leave
	ret
