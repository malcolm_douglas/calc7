/***********************************************************************
 *
 *
 *  this code takes a string of hex numbers, and outputs a string of hex
 *  numbers fit for including in a c-document (i.e, prefixed with "0x", 
 *  seperated by commas, broken into bytes.
 * 
 **********************************************************************/


#include <stdio.h>
#include <assert.h>

void push_char (unsigned char c)
{
    static int first = 0;
    if(!first) {
	first = 1;
    } else {
	printf(", ");
    }
    printf("0x%X", (unsigned int)c);
}

void push_hex_string(char *str)
{
    char *c;
    int state = 0;
    unsigned char tmp;
    unsigned char last_tmp;
    for(c = str ;*c != 0;c++) {
	if(isdigit(*c)) {
	    tmp = *c - '0';
	} else 	switch (*c) {
	case 'a':
	case 'b':
	case 'c':
	case 'd':
	case 'e':
	case 'f':
	    tmp = *c - 'a' + 10;
	    break;
	case 'A':
	case 'B':
	case 'C':
	case 'D':	       
	case 'E':
	case 'F':
	    tmp = *c - 'A' + 10;
	    break;
	case ' ':
	    assert(state == 0); //all bytes must be expressed by 2
	    //chars ("XX")
	    state = 3;
	    break;
	default:
	    assert(0);
	}
	if(state == 0) {
	    state = 1;
	    last_tmp = tmp;
	} else if (state == 1){
	    assert (state == 1);
	    tmp |= last_tmp << 4;
	    push_char(tmp);
	    state = 0;
	} else {
	    assert(state == 3);
	    state = 0;
	}
    }
}

int main(int argc, char **argv) 
{
    int i = 1;
    for(;i < argc; i++)
	push_hex_string (argv[i]);
	
    return 0;
}
	
