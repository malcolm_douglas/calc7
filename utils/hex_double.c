#include <stdio.h>
#include <string.h>

int main (int argc, char **argv)
{
    int i = 2;
    while(i <= argc) {
	int hi, lo;
	double f = strtod(argv[i-1], NULL);
	memcpy(&lo, &f, 4);
	memcpy(&hi, ((char *)&f + 4), 4);
	printf("%s: lo = 0x%x, hi = 0x%x ", argv[i-1], lo, hi);
	i++;
    }
    return 0;
}
