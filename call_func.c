#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <windows.h>

struct mc_buffer {
    char *buf;
    char *head;
    int buf_size;
};

void push_byte(unsigned char b, struct mc_buffer *buffy)
{
    push_char(b, buffy);
}

int write_code(struct mc_buffer *c)
{
    #ifdef USE_PUSH_BYTE
    //movl $5,  %eax
    push_byte(0xb8, c); //mov to eax
    push_byte(0x05, c); //5.....
    push_byte(0x00, c); //in
    push_byte(0x00, c); //little
    push_byte(0x00, c); //endian

    //ret
    //but is it near or far????
    //push_byte(0xcb, c); //far return.......???
    push_byte(0xc3, c); //near return.......???
#endif /*USE_PUSH_BYTE*/

    void push_hex_string(char *, struct mc_buffer *c);
    //    push_hex_string("B8 05 00 00 00 C3", c);
    
    
    return 0;
}

void dump_prog(char *filename, struct mc_buffer *prog)
{
    FILE *dump_file = fopen(filename, "w");
    char *h;
    for(h = prog->buf; h != prog->head; h++) {
	fputc (*h, dump_file);
    }
    fclose (dump_file);
}

void call_func(struct mc_buffer *buffy)
{
    dump_prog("dumpfile.hex", buffy);

    struct mc_buffer willow;
    memset(&willow, 0, sizeof(willow));
    write_code(&willow); 

    //    buffy = &willow;

    int size = buffy->head - buffy->buf;
    DWORD old_protection;
    LPVOID fancy_memory = VirtualAlloc(NULL, size,
		    MEM_COMMIT | MEM_RESERVE ,
		    PAGE_READWRITE );
    assert (fancy_memory);
    memmove(fancy_memory, buffy->buf, size);
    BOOL b = VirtualProtect(fancy_memory, size,
		    PAGE_EXECUTE, &old_protection);
    assert(b);
    double (*f_ptr)(void) = (double (*)(void))fancy_memory;
#ifdef STUTTER_LIKE_A_FOOL
    f_ptr();
    double d = f_ptr();
    int (*g_ptr)(void) = fancy_memory;
#endif
    printf("wait for it....  %lf\n", f_ptr());
}


void *get_func_mem(unsigned int size)
{
    return VirtualAlloc(NULL, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
}

BOOL set_func_mem_executable(void *mem, unsigned int size)
{
    DWORD old_protection;
    BOOL b = VirtualProtect(mem, size, PAGE_EXECUTE, &old_protection);
    return b;
}
