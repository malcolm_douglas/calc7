/*
 * support for the stuff where i am building a tree for the rpcalc program,
 * instead of executing commands as they occur.
 */

enum {V_NUM, V_FUNC};

struct mc_buffer {
    char *buf;
    char *head;
    int buf_size;
};


struct text_buffer {
    char *buf;
    char *head;
    int size;
};

struct function {
    char *name;
    struct mc_buffer *prog;
    int id;
    void (*f_ptr) (void);
    struct function *next;   /*simplest */
};

struct value;
typedef struct symtable {
    char ** table;
    int length;
    int total_size;
    struct value * valtable;
} symtable;
//this might be overly complicated, but symbols
//should be treated as opaque anyway.  
typedef struct symbol {
    int id;
    symtable *st;
} symbol;
//need some way to store expressions.
/*c_exp only appears in compiled expressions, which also use this enum.*/
struct comp_exp;
enum {NUM, PLUS, MINUS, MULT, DIV, POW, NEG, VAR, ASS};  
typedef struct expression {
    int type;
    struct expression *arg1, *arg2;
    union {
	double x;  //number
	symbol sym;
	struct comp_exp *c_exp;
    };
} expression;
struct proc;
struct if_clause;
struct loop;
enum {BLOCK, LINE, PROC, CALL, IF_CLAUSE, L_JUMP, L_LOOP, L_C_EXP};
typedef struct line {
    int type;
    struct line *next;
    union {
	expression *exp;
	struct comp_exp   *c_exp;
	struct line *body;
	struct proc *proc;
	struct if_clause *if_clause;
	struct loop *loop;
	symbol sym;
    };
} line;
typedef struct proc {
    symbol sym;
    line *block;
} proc;
typedef struct if_clause {
    expression *test;
    line *then_clause;
    line *else_clause;
} if_clause;
typedef struct loop {
    expression *test;
    line *body;
} loop;

typedef struct value {
    int type;
    union {
	double      x;
	int block;
	void (*func) (void);
    };
} value ;
typedef value (*built_in) (symtable *);

typedef struct comp_exp_stmt {
    int type;
    union {
	double x;
	symbol sym;
    };
} comp_exp_stmt;

typedef struct comp_exp {
    int size;
    comp_exp_stmt *end;
    comp_exp_stmt *start;
} comp_exp;

#define USE_C_ITEM_STRUCT
#ifdef USE_C_ITEM_STRUCT

/*c_item type tags */
enum {C_NUM= 0, C_PLUS = 1, C_MINUS = 2, C_MULT = 3, C_DIV = 4, C_POW = 5, 
      C_NEG = 6, C_VAR = 7, C_ASS = 8, C_BLOCK = 9, C_BLOCK_END = 10, 
      C_LINE = 11, C_PROC = 12, C_CALL = 13, C_IF = 14, C_ELSE = 15, C_JUMP = 16, 
      C_LOOP = 17, C_END_PROGRAM = 18 };

struct c_item {
    int type;
    union {
	/*NUMBER*/
	double x;
	/*operators need no info, just a type.... C_PLUS -- C_ASS */
	/* C_EXP just marks end of expresssion.  */
	/* C_BLOCK indicates end of block (start assumed to be next 
	   item.   I could have it just as book ends, but then I couldn't 
	   skip over them when needed. */
	int block;
	/* C_LINE just marks end of line; */
	/* C_PROC just saves the following block in the value table, skips block */
	/* C_CALL but doesn't need extra data */
	/* C_JUMP needs something, not sure what.  handle to a block prolly */
	/* C_LOOP and C_IF prolly need a bunch.  C_LOOp needs at lesat a handle
	   to the start of the expresssion, (or mark the beginning of it somehow)
	   oh wait... it could be C_LOOP [EXPRESSION STUFF] EXP BLOCK and that would have
	   enough info.  as i said not sure that's the best way.  if could be similar.  
	   actually, i'm not so sure I'm going to use the C_EXP to mark the end of expressions, 
	   cuz what's the point?  So a while loop will require somehow a start to the expression, 
	   and a start to the block.  But since a start block is recorded, and cannot be part of an 
	   expression, a while loop really just needs a "while" item to mark the beginning of the 
	   expression, the address of which can be taken by the interpretor, and then the next 
	   expression, and the next state, will be considered the test and the loop body.  
	   
	   tL;Dr: no extra data should be needed by the if then clauses.  
	*/
	symbol sym;  /*this is needed for C_VAR and C_ASS, and maybe C_PROC*/
    };
};

#endif /*USE_C_ITEM_STRUCT*/

struct prog_buf {
    struct c_item *tape;
    int size;
};


//this checks for overflows.  yay  
void write_c_item (struct c_item *, struct prog_buf *, int head);

/*comp_exp *compile_expression (expression *);*/
/*void push_c_exp(comp_exp *stack, comp_exp_stmt *push_me);*/
void destroy_expression(expression *);
void init_symtable(symtable *);
void init_valtable(symtable *, value **);
symbol get_symbol_from_string(char *, symtable *);
char * get_string_from_symbol(symbol);
double get_value (symbol);
void set_value(symbol, double);
void set_proc(symbol s, int block);
void set_fun(symbol, built_in);
void run_proc(symbol);
value lookup_symbol(symbol);
line *append_line (line *list, line *addme);
expression *get_expression_from_num(double x);
expression *get_expression_from_exp(int type, expression *a1, expression *a2);
expression *get_expression_from_sym(symbol);
expression *get_expression_from_c_exp(struct comp_exp *);
expression *get_assignment(symbol, expression *);
line *get_line (expression *exp);
line *get_line_from_proc(proc *);
line *get_block (line *l);
line *get_line_from_call(symbol);
line *get_line_from_jump(symbol);
proc *get_proc(symbol, line *block);
if_clause *get_if_clause(expression *, line *, line *);
line *get_line_from_if(if_clause *);
loop *get_loop(expression *, line *);
line *get_line_from_loop(loop *);
void install_builtins(symtable *);
void declare_builtins(symtable *);
struct mc_buffer * compile(line *);
comp_exp *stackify_expression(expression *exp);

void push_char (unsigned char c, struct mc_buffer *prog);
void push_buffer (unsigned char *buf, int size, struct mc_buffer *prog);
void push_hex_string(char *str, struct mc_buffer *prog);
void push_uint (unsigned int x,struct  mc_buffer *prog);

void call_func(struct mc_buffer *prog);
void *get_func_mem(unsigned int size);
int set_func_mem_executable(void *mem, unsigned int size);
void dump_prog(char *filename, struct mc_buffer *prog);
