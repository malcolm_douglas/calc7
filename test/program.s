
	.text
	.globl _program1
_program1:
	enter $0, $0
	pushl $0x40000000
	pushl $0x0
	pushl $0x40000000
	pushl $0x0
	fldl (%esp)
	fldl 8(%esp)
	fadd %st(1), %st(0)
	subl $8, %esp
	fstl (%esp)
	pushl $0x40140000
	pushl $0x0
	fldl (%esp)
	fldl 8(%esp)
	fsub %st(1), %st(0)
	subl $8, %esp
	fstl (%esp)
	movl $0, %eax
	leave
	ret
