#include <stdio.h>

extern double program1(void);

int main()
{
    double x = program1();
    printf("%lf\n", x);
    return 0;
}
