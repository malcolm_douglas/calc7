	.data
	.globl _arg1
	.align 4
_arg1:
	.long 2
	.globl _mod
	.align 4
_mod:
	.long 3
	.globl _mod_part
	.align 4
_mod_part:
	.long 4
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 5
	.globl _print_num
	.align 4
_print_num:
	.long 6

	.comm _main_valtable, 112

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x40140000
	pushl $0x0
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	pushl $0x40000000
	pushl $0x0
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
L0:
	movl $0, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	fldl (%esp)
	addl $8, %esp
	fldz
	fcomip
	ffree %st(0)
	je L1
	pushl %ebp
	movl %esp, %ebp
	movl $0, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	pushl $0x3ff00000
	pushl $0x0
	fldl (%esp)
	fldl 8(%esp)
	fsubp %st(0), %st(1)
	addl $8, %esp
	fstpl (%esp)
	ffree %st(0)
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	fldl (%esp)
	fldl 8(%esp)
	faddp %st(0), %st(1)
	addl $8, %esp
	fstpl (%esp)
	ffree %st(0)
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	leave
jmp L0
L1:
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	pushl $0x0
	pushl $0x0
	fldl (%esp)
	fldl 8(%esp)
	faddp %st(0), %st(1)
	addl $8, %esp
	fstpl (%esp)
	ffree %st(0)
	movl $0, %eax
	leave
	ret
