	.data
	.globl _arg1
	.align 4
_arg1:
	.long 3
	.globl _mod
	.align 4
_mod:
	.long 4
	.globl _mod_part
	.align 4
_mod_part:
	.long 5
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 6
	.globl _print_num
	.align 4
_print_num:
	.long 7

	.comm _main_valtable, 128

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	pushl $0x40240000
	pushl $0x0
	movl $0, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	pushl $0x402e0000
	pushl $0x0
	movl $16, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	movl $0, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	fldl (%esp)
	fldl 8(%esp)
	fadd %st(1), %st(0)
	addl $8, %esp
	fstl (%esp)
	movl $32, %eax
	addl $_main_valtable, %eax
	movl (%esp), %ebx
	movl %ebx, 4(%eax)
	movl 4(%esp), %ebx
	movl %ebx, 8(%eax)
	movl $32, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	movl $16, %eax
	addl $_main_valtable, %eax
	pushl 8(%eax)
	pushl 4(%eax)
	fldl (%esp)
	fldl 8(%esp)
	fadd %st(1), %st(0)
	addl $8, %esp
	fstl (%esp)
	movl $0, %eax
	leave
	ret
