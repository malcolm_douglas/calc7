CFLAGS=-g -fno-strict-aliasing
OBJS=stackify_exp.o compile.o main.o parser.o lexor.o call_func.o

calc.exe: ${OBJS}
	gcc -o calc.exe ${OBJS}

parser.c: parser.y
	bison -o parser.c --defines=parser.h calc.y

lexor.o: lexor.c parser.c 
