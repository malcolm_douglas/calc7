/*
ok, i am editing this to create what i think is called an AST, instead of just executing code.
Then, on error, the result will be printed.

ok, that worked!  yay.  Now the trick is to switch from reverse polish to infix, by just editing the
grammer.  
*/

     %{

       #include <math.h>
       #include <stdio.h>
       #include <stdlib.h>      
       #include "calc.h"
       extern FILE *input_file;
       extern symtable main_symtable;
       extern line *first_line;
       int yylex (void);
       void yyerror (char const *);

     %}

     %union {
        double       val;
        expression  *exp;
        line       *line;
	symbol       sym;
     }

     %token <val> VAL
     %token <sym> SYM
     %token <sym> DEFUN
     %token <sym> RUN
     %token <sym> IF
     %token <sym> ELSE
     %token <sym> JUMP
     %token <sym> WHILE
     
     %type <exp> exp
     %type <line> line
     %type <line> lines
     %type <line> block

     %right '='
     %left '+' '-'
     %left '*' '/'
     %left NEGATE
     %right '^'
     %nonassoc DEFUN
     %nonassoc RUN
     %nonassoc JUMP
    

     %% /* Grammar rules and actions follow.  */

    input:
     /*empty */       
     | lines   { first_line = $1; }
  /*   | input block */
     ;
    
    lines:
      line             {$$ = $1}
    | lines line       {$$ = append_line($1, $2); }
    | lines block      {$$ = append_line($1, $2); }
    ;

     line:
       '\n'                  { $$ = get_line (NULL); }
     | exp '\n'              { $$ = get_line ($1); }
     | SYM block %prec DEFUN { $$ = get_line_from_proc( get_proc($1, $2)); }
     | RUN SYM               { $$ = get_line_from_call( $2 ); } 
     | JUMP SYM              { $$ = get_line_from_jump( $2 ); }
     | IF '(' exp ')' block  { $$ = get_line_from_if(get_if_clause($3, $5, NULL)); }
     | IF '(' exp ')' block ELSE block 
			     { $$ = get_line_from_if(get_if_clause($3, $5, $7)); }
     | WHILE '(' exp ')' block  {$$ = get_line_from_loop(get_loop($3, $5)); }

     ;

    block:
      '{' '}'            {$$ = get_block(NULL);}
    | '{' lines  '}'     {$$ = get_block($2);}
    | '{' exp    '}'     {$$ = get_block(get_line($2));}
    | '{' lines exp '}'  {$$ = get_block(append_line($2, get_line($3)));}
    ;
    
     exp:
       VAL                   { $$ = get_expression_from_num($1);               }
     | exp '+' exp           { $$ = get_expression_from_exp(PLUS,  $1,   $3);  }
     | exp '-' exp           { $$ = get_expression_from_exp(MINUS, $1,   $3);  }
     | exp '*' exp           { $$ = get_expression_from_exp(MULT,  $1,   $3);  }
     | exp '/' exp           { $$ = get_expression_from_exp(DIV,   $1,   $3);  }
     | exp '^' exp           { $$ = get_expression_from_exp(POW,   $1,   $3);  }
     | '-' exp %prec NEGATE  { $$ = get_expression_from_exp(NEG,   $2, NULL);  }
     | '(' exp ')'           { $$ = $2; }
     | SYM '=' exp           { $$ = get_assignment($1, $3); }
     | SYM                   { $$ = get_expression_from_sym($1); }
     
     ;
     %%

