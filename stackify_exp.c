#include "calc.h"
#include <stdlib.h>
#include <string.h>
//this is all stuff from calc3, it's to help build stack based asm
//instructions.

#ifdef USE_CALC3_DEFINES

enum {NUM, PLUS, MINUS, MULT, DIV, EXP, NEG, VAR, ASS, C_EXP}; 

typedef struct expression {
    int type;
    struct expression *arg1, *arg2;
    union {
	double x;  //number
	symbol sym;
	struct comp_exp *c_exp;
    };
} expression;

#endif /* USE_CALC3_DEFINES */

void push_c_exp(comp_exp *stack, comp_exp_stmt *stmt)
{
    if(stack->start == NULL) {
	stack->size = 20;
	stack->start = stack->end = malloc(sizeof(comp_exp_stmt) * stack->size);
    } else if(stack->end - stack->start == stack->size) {
	stack->start = realloc(stack->start, stack->size * 2);
	stack->end = stack->start + stack->size;
	stack->size *= 2;
    }
    *stack->end = *stmt;
    stack->end++;
}

comp_exp *stackify_sub_expression(expression *exp, comp_exp *c_exp)
{
    comp_exp_stmt s;
    if(!exp)
	return c_exp;
    if(exp->type == NUM) {
	s.type = NUM;
	s.x = exp->x;
	push_c_exp (c_exp, &s);
	return c_exp;
    } else if (exp->type == ASS) {
	stackify_sub_expression(exp->arg1, c_exp);
	s.type = ASS;
	s.sym = exp->sym;
	push_c_exp(c_exp, &s);
	return c_exp;
    } else if (exp->type == VAR) {
	s.type = VAR;
	s.sym = exp->sym;
	push_c_exp(c_exp, &s);
	return c_exp;
    } else if (exp->type == NEG) {
	stackify_sub_expression(exp->arg1, c_exp);
	s.type = NEG;
	push_c_exp(c_exp, &s);
    } else {
	stackify_sub_expression(exp->arg1, c_exp);
	stackify_sub_expression(exp->arg2, c_exp);
	s.type = exp->type;
	push_c_exp(c_exp, &s);
    }
}
	    
comp_exp *stackify_expression(expression *exp)
{
    comp_exp *c_exp = malloc(sizeof(comp_exp));
    memset(c_exp, 0, sizeof(comp_exp));
    
    return stackify_sub_expression(exp, c_exp);
}
