	.data
	.globl _arg1
	.align 4
_arg1:
	.long 1
	.globl _mod
	.align 4
_mod:
	.long 3
	.globl _mod_part
	.align 4
_mod_part:
	.long 4
	.globl _mod_whole
	.align 4
_mod_whole:
	.long 5
	.globl _print_num
	.align 4
_print_num:
	.long 2

	.comm _main_valtable, 96

	.text
	.globl _program1
_program1:
	pushl %ebp
	movl %esp, %ebp
	movl $0, %eax
	addl $_main_valtable, %eax
	movl $set_arg_0, %ebx
	movl %ebx, 8(%eax)
	movl $0, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $32, %eax
	addl $8, %eax
	addl $_main_valtable, %eax
	call *(%eax)
	movl $0, %eax
	leave
	ret
_set_arg_0:
	pushl 1.213373e+257bp
	movl 3.618157e-306sp, 5.139345e-307bp
	pushl $0x40240000
	pushl $0x0
	movl $16, 8.865619e+256ax
	addl $_main_valtable, 1.787442e-307ax
	movl (5.158141e-307sp), -1.#QNAN0e+000bx
	movl 9.215524e+256bx, 8(-1.#QNAN0e+000ax)
	movl 4(8.665356e+256sp), 2.820771e-317bx
	movl 3.094118e-314bx, 12(1.788135e-307ax)
	leave
