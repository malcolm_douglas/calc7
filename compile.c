#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>

#include "calc.h"

struct function *funcs = NULL;
char buffer[1024];  

extern value * main_valtable;

void mangle_name(char *name)
{
    char buffy[100];
    static unsigned int count = 0;
    sprintf(buffy, "_%d", count++);
    strcat(name, buffy);
}

void enlarge_text_buffer(struct text_buffer *text)
{
    int tmp = text->head - text->buf;
    if(text->size == 0) {
	tmp = 0;
	text->size = 1024;
	text->buf = NULL;
    } else {
	text->size *= 2;
    }
    text->buf = realloc(text->buf, text->size);
    text->head = text->buf + tmp;
}

int write_text (char *str, struct text_buffer *text)
{
    int length = strlen(str);
    while((text->head - text->buf) + length >= text->size) {
	enlarge_text_buffer(text);
    }
    strncpy(text->head, str, length);
    text->head += length;
    *text->head = '\0';
    return length;
}
int write_inst(char *inst, struct text_buffer *text)
{
    write_text("\t", text);
    write_text(inst, text);
    write_text("\n", text);
}

#ifdef USE_387_FPOINT
    #define write_text_387(a, b) write_text(a, b)
    #define write_inst_387(a, b) write_inst(a, b)
    #define write_text_SSE(a, b)
    #define write_inst_SSE(a, b)
#else
    #define write_text_387(a, b) 
    #define write_inst_387(a, b) 
    #define write_text_SSE(a, b) write_text(a, b)
    #define write_inst_SSE(a, b) write_inst(a, b)
#endif

int compile_function (line *proc, struct mc_buffer *prog)
{
    char willow[1024];  //overkill.....
    char *tmp;
    
    struct function *newf = malloc(sizeof(struct function));
    memset(newf, 0, sizeof(struct function));
    
    tmp = get_string_from_symbol(proc->proc->sym); 
    strcpy(willow, tmp);
    free(tmp);
    mangle_name(willow);
    tmp = malloc(strlen(willow) + 1);
    assert(tmp);
    strcpy(tmp, willow);
    
    newf->name = tmp;
    newf->id = proc->proc->sym.id;
    newf->prog = malloc(sizeof(struct mc_buffer));
    memset(newf->prog, 0, sizeof(struct mc_buffer));
    // (push-hex-string "enter" "C8 00 00 00")
    unsigned char enter[] = {0xC8, 0x0, 0x0, 0x0};
    push_buffer (enter, sizeof(enter), newf->prog);

    compile_block(proc->proc->block, newf->prog);
    //    write_inst("ret", newf->body);
    // leave
    push_char(0xC9, newf->prog);
    // ret (near)
    push_char(0xC3, newf->prog);
    
    //o#define JUST_CALL_IT    
#ifdef JUST_CALL_IT
    call_func(newf->prog);
    exit(0);
#endif /*JUST_CALL_IT*/

    //not threadsafe...
    newf->next = funcs;
    funcs = newf;

    unsigned int f_size = newf->prog->head - newf->prog->buf;
    void *f_mem = get_func_mem(f_size);
    memmove(f_mem, newf->prog->buf, f_size);
    set_func_mem_executable(f_mem, f_size);
    newf->f_ptr = f_mem;
    strcpy(willow, newf->name);
    strcpy(willow + strlen(willow), "_dump.hex");
    dump_prog(willow, newf->prog);
    free (newf->prog->buf);
    memset(newf->prog, 0, sizeof(struct mc_buffer));

/* here is where we load it up....*/
#ifdef OLD_CODE_IS_OLD
    sprintf(willow, 
	    /*"__S_%d->type = V_FUNC,*/
	    "\tmovl $%d, %%eax\n",
	    newf->id * sizeof(value));
    write_text(willow, buffy);
    write_inst("addl $_main_valtable, %eax", buffy);
    sprintf(willow, 
	    /*"__S_%d->type = V_FUNC,*/
	    "\tmovl $_%s, %%ebx\n",
	    newf->name);
    write_text(willow, buffy);
    write_inst("movl %ebx, 8(%eax)", buffy);
#endif /*OLD_CODE_IS_OLD*/

    //movl newf->id * sizeof(value) + main_valtable %eax
    push_char((char)0xB8, prog);
    push_uint((unsigned int) newf->id * sizeof(value) +
	      (unsigned int) main_valtable, prog);
    //movl newf->f_ptr %ebx
    push_char(0xB8 + 3, prog); //mov immediate to ebx
    push_uint((unsigned int) newf->f_ptr, prog);
    //movl %ebx, 8(%eax)
    //(push-hex-string "set_f_ptr" "89 58 08")
    unsigned char set_f_ptr[] = {0x89, 0x58, 0x8};
    push_buffer (set_f_ptr, sizeof(set_f_ptr), prog);

    //ret at some point 
    return 1;
}

int write_binary_op (int op, struct text_buffer *text)
{
    write_text_387("\tfldl (%esp)\n", text);
    write_text_387("\tfldl 8(%esp)\n", text);
    write_inst_SSE("movsd 8(%esp), %xmm0", text);
    write_inst_SSE("movsd (%esp), %xmm1", text);
    switch(op) {
    case PLUS:
	write_inst_387("faddp %st(0), %st(1)", text);
        write_inst_SSE("addsd %xmm1, %xmm0", text);
	break;
    case MINUS:
	write_inst_387("fsubp %st(0), %st(1)", text);
	write_inst_SSE("subsd %xmm1, %xmm0", text);
	break;
    case MULT:
	write_inst_SSE("mulsd %xmm1, %xmm0", text)
	write_inst_387("fmulp %st(0), %st(1)", text);
	break;
    case DIV:
	write_inst_387("fdivp %st(0), %st(1)", text);
	write_inst_SSE("divsd %xmm1, %xmm0", text);
	break;
    default:
	assert(0);
    }
    write_inst("addl $8, %esp", text);  //pop it off...
    write_inst_387("fstpl (%esp)", text);
    write_inst_387("ffree %st(0)", text);
    write_inst_SSE("movsd %xmm0, (%esp)", text);
}

void push_char (unsigned char c, struct mc_buffer *prog)
{
    int size = prog->head - prog->buf;
    if(size >= prog->buf_size) {
	if(prog->buf_size) {
	    prog->buf_size *=2;
	}
	else 
	    prog->buf_size = 1024;
	
	prog->buf = realloc(prog->head, prog->buf_size);
	//FIXME:  this bad practice
	assert(prog->buf);
	prog->head = prog->buf + size;
    }
    *(prog->head++)=c;
}

void push_buffer (unsigned char *buf, int size, struct mc_buffer *prog)
{
    int i;
    for(i = 0; i < size; i++) 
	push_char (buf[i], prog);
}

void push_hex_string(char *str, struct mc_buffer *prog)
{
    char *c;
    int state = 0;
    unsigned char tmp;
    unsigned char last_tmp;
    for(c = str ;*c != 0;c++) {
	if(isdigit(*c)) {
	    tmp = *c - '0';
	} else 	switch (*c) {
	case 'a':
	case 'b':
	case 'c':
	case 'd':
	case 'e':
	case 'f':
	    tmp = *c - 'a' + 10;
	    break;
	case 'A':
	case 'B':
	case 'C':
	case 'D':	       
	case 'E':
	case 'F':
	    tmp = *c - 'A' + 10;
	    break;
	case ' ':
	    assert(state == 0); //all bytes must be expressed by 2
	    //chars ("XX")
	    state = 3;
	    break;
	default:
	    assert(0);
	}
	if(state == 0) {
	    state = 1;
	    last_tmp = tmp;
	} else if (state == 1){
	    assert (state == 1);
	    tmp |= last_tmp << 4;
	    push_char(tmp, prog);
	    state = 0;
	} else {
	    assert(state == 3);
	    state = 0;
	}
    }
}

void push_uint (unsigned int x,struct  mc_buffer *prog)
{
    int i;
    unsigned char *c = (void *) &x;
    for( i = 0; i < 4; i++)
	push_char(*(c+i), prog);
}
    

int push_binary_op (int op, struct mc_buffer *prog)
{
    /*
     * FIXME : this code is unnecessaryily putting both operands to
     * registers, one could remain in memory.  probably would not have
     * much influence on performance, not sure though.
     */

    /*mov %esp, %eax */
    /* can't use esp for this simply, so I'll just move it to eax. */
    /* the alternative is to use the base offset bit thingy, which
       does allow reference to esp. */
    /* 89--reg to r/m */
    
    /* mov %esp, %eax */
    push_hex_string("89 E0", prog);
    /*movsd 8(%eax), %xmm0 */
    push_hex_string("F2 0F 10 40 08", prog); /*3 byte opcode movsd, modr/m
					 byte , 8 bit displacement of 8*/
    
    /*movsd (%esp), %xmm1*/
    push_hex_string("F2 0F 10 08", prog);  /* 3 byte opcode for movsd, modr/m byte */

    switch(op) {
    case PLUS:
	/*addsd %xmm1, %xmm0 */
	push_hex_string("F2 0F 58 C1", prog);  //c1 is the modr/m byte
					       //for all of these
					       //opps, since they all
					       //go from xmm1(r/m) to
					       //xmm0(reg)
	break;
    case MINUS:
	/*subsd %xmm1, %xmm0 */
	push_hex_string("F2 0F 5C C1", prog);
	break;
    case MULT:
	/*mulsd %xmm1, %xmm0*/
	push_hex_string("F2 0F 59 C1", prog);
	break;
    case DIV:
	/*divsd %xmm1, %xmm0*/
	push_hex_string("F2 0F 5E C1", prog);
	break;
    default:
	assert(0);
    }
    /*addl $8, %esp  : pop it off */
    push_hex_string("83 C4 08", prog);  
    /*movsd %xmm0, (%esp) */
    push_hex_string ("F2 0F 11 40 08", prog);
}

		
int compile_expression(expression *exp, struct  mc_buffer *prog)
{

    char buffy[1024];
    value v;
    comp_exp *ce;
    memset(&ce, 0, sizeof(ce));
    ce = stackify_expression(exp);
    comp_exp_stmt *head = NULL;
    
    struct text_buffer *text;

    for(head = ce->start; head < ce->end; ++head) {
	switch(head->type) {
	case NUM:
	    {

		unsigned int hi, lo;
		memcpy(&lo, &head->x, sizeof(hi));
		memcpy(&hi, ((char *)&head->x) + sizeof(hi), sizeof(lo));

#ifdef CALC7_REFERENCE_CODE
		sprintf(buffy, "\tpushl $0x%x\n", hi);
		write_text(buffy, text);
		sprintf(buffy, "\tpushl $0x%x\n", lo);
		write_text(buffy, text);
#endif /*CALC7_REFERENCE_CODE*/
		/* push hi */
 		push_hex_string("68", prog);
		push_uint(hi, prog);
		push_hex_string("68", prog);
		push_uint(lo, prog);
	    }
	    break;
	case PLUS:
	case MULT:
	case DIV:
	case MINUS:
	    push_binary_op(head->type, prog);
	    break;
	case NEG:
	    write_text_387("\tfldl (%esp)\n", text);
	    write_text_387("\tfchs st(0)\n", text);
	    write_text_387("\tfstpl (%esp)\n", text);

	    write_inst_SSE("movsd (%esp), %xmm0", text);
	    write_inst_SSE("pushl $0x80000000", text);
	    write_inst_SSE("pushl $0x0", text);
	    write_inst_SSE("movsd (%esp), %xmm1", text);
	    write_inst_SSE("addl $8, %esp", text);
	    write_inst_SSE("xorpd %xmm1, %xmm0", text);
	    write_inst_SSE("movsd %xmm0,(%esp)", text);
	    
	    break;
	case ASS:
#ifdef USE_ASSEMBLER_LIKE_A_NINNNY
	    sprintf(buffer, "\tmovl $%d, %%eax\n", head->sym.id*sizeof(value));
	    write_text(buffer, text);
	    write_inst("addl $_main_valtable, %eax", text);
	    write_inst("movl (%esp), %ebx", text);
	    write_inst("movl %ebx, 8(%eax)", text);
	    write_inst("movl 4(%esp), %ebx", text);
	    write_inst("movl %ebx, 12(%eax)", text);
	    break;
#endif
	    //ok, load up the variable.  
	    //
	    {
		//i am starting to wonder if it wouldn't be better to
		//just use the push hex string function in the prgram,
		//this is a bit hard to read.

		//mov (sym.id * sizeof(value)), %eax
		//(assign-to-hex-string "inst_1" "B8")
		unsigned char inst_1[] = {0xB8};
		push_buffer(inst_1, sizeof(inst_1), prog);
		push_uint(head->sym.id * sizeof(value), prog);
		//NOTE: main_valtable must be set before compiling,
		//but after parsing.
		
		//addl main_valtable, %eax
		//(push-hex-string "add_addr" "81 C0")
		unsigned char add_addr[] = {0x81, 0xC0};
		push_buffer (add_addr, sizeof(add_addr), prog);
		push_uint((unsigned int) main_valtable, prog);
		//movl (%esp), ebx
		//movl %ebx, 8(%eax)
		//(push-hex-string "push_lo" "8b1C24 895808")
		unsigned char push_lo[] = {0x8B, 0x1C, 0x24, 0x89, 0x58, 0x8};
		push_buffer (push_lo, sizeof(push_lo), prog);

		//movl 4(%esp), ebx
		//movl %ebx, 12(%eax)
		//(push-hex-string "push_hi" "8b5C2404 89580C")
		unsigned char push_hi[] = {0x8B, 0x5C, 0x24, 0x4, 0x89, 0x58, 0xC};
		push_buffer (push_hi, sizeof(push_hi), prog);

		break;
	    }

	case VAR:
#ifdef ASSEMBLY_IS_FOR_WIMPS
	    sprintf(buffer, "\tmovl $%d, %%eax\n", head->sym.id*sizeof(value));
	    write_text(buffer, text);
	    write_inst("addl $_main_valtable, %eax", text);
	    write_inst("pushl 12(%eax)", text);
	    write_inst("pushl 8(%eax)", text);
	    break;
	    
#endif 
	    {
		//movl {head->sym.id * sizeof(value)}, %eax
		//(push-hex-string "set_addr" "B8")
		unsigned char set_addr[] = {0xB8};
		push_buffer (set_addr, sizeof(set_addr), prog);
		push_uint((unsigned int)(head->sym.id * sizeof(value)), prog);
		//addl main_valtable, %eax
		push_char(0x05, prog); //add to eax is 05
		push_uint((unsigned int) main_valtable, prog);
		//push 12(%eax)
		//push 8(eax)
		//(push-hex-string "push_it" "FF700C FF7008")
		unsigned char push_it[] = {0xFF, 0x70, 0xC, 0xFF, 0x70, 0x8};
		push_buffer (push_it, sizeof(push_it), prog);
		break;
	    }
	case POW:
	    //this is done using 2 op's that allow you to get
	    //2^(y * log2(x)), to efficiently compute x^y
	    assert(0);
	    break;
	default:
	    assert(0);
	    break;
	}
    }
    free(ce->start);
    return 1;
}
void init_prog_buf(struct prog_buf *buffy) 
{
    buffy->size = 1024;
    buffy->tape = malloc(sizeof(struct c_item) * buffy->size);
    assert(buffy->tape);
}
int compile_loop(line *, struct mc_buffer *prog);

//blocks have no real meaning in asm...  i suppose i could push the
//stack with an enter instruction (or the equivalent), but Im not sure
//i know what the point of that would be...

int compile_block(line *l, struct mc_buffer *prog)
{
    struct c_item s;
    int tmp;

    // I'm just going to go with enter $0 $0.....
    // write_inst("pushl %ebp", text);
    // write_inst("movl %esp, %ebp", text);
    // (push-hex-string "enter" "C8 00 00 00")<
    unsigned char enter[] = {0xC8, 0x0, 0x0, 0x0};
    push_buffer (enter, sizeof(enter), prog);
    compile_loop(l->body, prog);
    //write_inst("leave", text);    
    //leave, no need for ret....
    //(push-hex-string "buffy" "C9")
    unsigned char buffy[] = {0xC9};
    push_buffer (buffy, sizeof(buffy), prog);
    return 0;
}

char *get_new_label (void)
{
    static int n = 0;
    char buffer[80];
    char *s;
    sprintf(buffer, "L%d", n);
    int len = strlen(buffer);
    s = malloc(len + 1);
    strncpy(s, buffer, len);
    s[len] = '\0';
    n++;
    return s;
}

int compile_loop(line *l, struct mc_buffer *prog)
{   
    struct text_buffer *text;
    while(l) {
	switch(l->type) {
	case BLOCK:
	    compile_block(l, prog);
	    break;
	case LINE:
	    if(l->exp == NULL)
		break;
	    compile_expression(l->exp, prog);
	    //	    write_text(";\n", text);
	    break;
	case PROC:
	    compile_function(l, prog);
	    break;
	case CALL:
#ifdef OLD_CALL
	    sprintf(buffer, "\tmovl $%d, %%eax\n", l->sym.id * sizeof(value));
	    write_text(buffer, text);
	    write_inst("addl $8, %eax", text);
	    write_inst("addl $_main_valtable, %eax", text);
	    write_inst("call *(%eax)", text);
	    break;
#endif /*OLD_CALL*/
	    //ok, have to use absolute indirect addressing, because, I
	    //don't have the address of the instruction, since it is
	    //made afterwards and copied over.  
	    //
	    //FIXME: the memory should be allocated first, then
	    //written in, then made to be executable.  Doing it this
	    //way is not only an extra copy, but also results in
	    //suboptimal code, since relative calls can't be used.
	    //
	    //movl (l->sym.id
	    //*sizeof(value) + main_variable), %eax
	    push_char(0xb9, prog);
	    push_uint((unsigned int)(l->sym.id * sizeof(value) +
				    (unsigned int) main_valtable) + 8, prog);
	    //movl (%ecx), eax
	    //(push-hex-string "mov_ecx_eax" "8B 01")
	    //unsigned char mov_ecx_eax[] = {0x8B, 0x1};
	    //push_buffer (mov_ecx_eax, sizeof(mov_ecx_eax), prog);

	    //call *(%ecx)
	    //(push-hex-string "call_ecx" "FF 11")
	    { unsigned char call_ecx[] = {0xFF, 0x11};
		push_buffer (call_ecx, sizeof(call_ecx), prog);}

	    break;
	    
	    /*****************************************************************
	     *
             *  ok, the tricky part of this will be keeping track of
             *  the "labels", since everthing has to be given in
             *  reletave addressing.  Which in a way is ok, its
             *  doable.  Just save the prog->head pointers at the
             *  appropriate place.
             *
             *  remember, jmp is from the instruction following the
             *  jmp instruction
             *
             *  also unfortunate : I will have to go back and fill in
             *  the value of the relative address for the loop exit
             *  from a failed test.
             *
             ****************************************************************/

	case L_LOOP:
	    {

		int printf ( const char * format, ... );
		int (*my_printf)(const char *, ...) = printf;
		char *fmat_string = "Ok, here it is: %lf, %lf \n";
		char *top_of_loop = prog->head;
		compile_expression(l->loop->test, prog);
		//pushl $0 
		//pushl $0
		//(push-hex-string "push0" "68")
		{ unsigned char push0[] = {0x68};
		    push_buffer (push0, sizeof(push0), prog);}
		push_uint(0x00000000, prog);
		{ unsigned char push0[] = {0x68};
		    push_buffer (push0, sizeof(push0), prog);}
		push_uint(0x00000000, prog);
		//ok, I'm pushing a call to printf now, checking to
		//see what the deal is.  didn't show anything.  damn.  
		//mov imm ebx
		//(push-hex-string "mov_to_eax"
		//call *(%eax)
		//(push-hex-string "call" "FF 10")
		//movsd (%esp) %xmm0
		//(push-hex-string "movsd" "F20f10 0424")
		{ unsigned char movsd[] = {0xF2, 0xF, 0x10, 0x4, 0x24};
		    push_buffer (movsd, sizeof(movsd), prog);}
		//addl $8, esp
		//(push-hex-string "add_8" "81C4")
		{ unsigned char add_8[] = {0x81, 0xC4};
		    push_buffer (add_8, sizeof(add_8), prog);}
		push_uint(8, prog);
		//movsd (%esp) xmm1
		//(push-hex-string "pop_xmm1" "F20F10 0C24")
		{ unsigned char pop_xmm1[] = {0xF2, 0xF, 0x10, 0xC, 0x24};
		    push_buffer (pop_xmm1, sizeof(pop_xmm1), prog);}
		//COMISD xmm0, xmm1
		//(push-hex-string "comp" "660f2fc1")
		{ unsigned char comp[] = {0x66, 0xF, 0x2E, 0xC1};
		    push_buffer (comp, sizeof(comp), prog);}
		//je
		////(push-hex-string "je" "0F84")
		{ unsigned char je[] = {0xF, 0x84};
		    push_buffer (je, sizeof(je), prog);}
		char *jump_cd_from_test = prog->head;
		push_uint(0XFFFFFFFF, prog);
		char *jump_from_here_test = prog->head;
		compile_block(l->loop->body, prog);
		//jmp
		push_char(0xE9, prog);
		//ok, now the tricky part.  
		char *bottom_of_loop = prog->head + 4;
		int jump_up = top_of_loop - bottom_of_loop;
		push_uint(jump_up, prog);
		int escape_down = bottom_of_loop -jump_from_here_test;
		memmove(jump_cd_from_test, &escape_down, 4);
		break;		
	       
		//well, I think that's everything.  probably messed up
		//somewhere though.

#ifdef OLD_LOOP_CODE 

		char *test, *loop_exit;
		test = get_new_label();
		loop_exit = get_new_label();

		write_text(test, text);
		write_text(":\n", text);
		compile_expression(l->loop->test, text);

		write_inst_387("fldl (%esp)", text);
		write_inst_387("addl $8, %esp", text);  //add to pop off I think...
		write_inst_387("fldz", text);
		write_inst_387("fcomip", text);
		write_inst_387("ffree %st(0)", text);

		write_inst_SSE("pushl $0", text);
		write_inst_SSE("pushl $0", text);
		write_inst_SSE("movsd (%esp), %xmm0", text);
		write_inst_SSE("addl $8, %esp", text);
		write_inst_SSE("movsd (%esp), %xmm1", text);
		write_inst_SSE("ucomisd %xmm0, %xmm1", text);
		
		write_text("\tje ", text);
		write_text(loop_exit, text);
		write_text("\n", text);
		compile_block(l->loop->body, text);
		write_text("jmp ", text);
		write_text(test, text);
		write_text("\n", text);
		write_text(loop_exit, text);
		write_text(":\n", text);

		free(test);
		free(loop_exit);
#endif /*OLD_LOOP_CODE */
	    }
	    break;
	case IF_CLAUSE:
	    {
		int need_else = l->if_clause->else_clause ? 1 : 0;
		
		//the test code is based on the code in the while
		//section, which has some comments.
		compile_expression(l->if_clause->test, prog);		
		//(push-hex-string "push0" "68")
		{ unsigned char push0[] = {0x68};
		    push_buffer (push0, sizeof(push0), prog);}
		push_uint(0x00, prog);
		{ unsigned char push0[] = {0x68};
		    push_buffer (push0, sizeof(push0), prog);}
		push_uint(0x00, prog);
		//(push-hex-string "movsd" "F20f10 0424")
		{ unsigned char movsd[] = {0xF2, 0xF, 0x10, 0x4, 0x24};
		    push_buffer (movsd, sizeof(movsd), prog);}
		//(push-hex-string "add_8" "81C4")
		{ unsigned char add_8[] = {0x81, 0xC4};
		    push_buffer (add_8, sizeof(add_8), prog);}
		push_uint(8, prog);
		//(push-hex-string "pop_xmm1" "F20F10 0C24")
		{ unsigned char pop_xmm1[] = {0xF2, 0xF, 0x10, 0xC, 0x24};
		    push_buffer (pop_xmm1, sizeof(pop_xmm1), prog);}
		//COMISD xmm0, xmm1
		//(push-hex-string "comp" "660f2fc1")
		{ unsigned char comp[] = {0x66, 0xF, 0x2E, 0xC1};
		    push_buffer (comp, sizeof(comp), prog);}
		//je
		////(push-hex-string "je" "0F84")
		{ unsigned char je[] = {0xF, 0x84};
		    push_buffer (je, sizeof(je), prog);}
		char *jump_cd_from_test = prog->head;
		push_uint(0XFFFFFFFF, prog);
		char *jump_from_here_test = prog->head;
		//ok, that is it for the while code cut and paste...
		compile_block(l->if_clause->then_clause, prog);
		char *end_of_then_clause = prog->head;
		char *then_clause_insert = NULL;
		if(need_else) {
		    //jmp
		    push_char(0xE9, prog);
		    then_clause_insert = prog->head;
		    push_uint(0XFFFFFFFF, prog);
		    end_of_then_clause = prog->head;
		}
		int jmp_distance = end_of_then_clause - jump_from_here_test;
		memmove(jump_cd_from_test, &jmp_distance, 4);
		if(need_else) {
		    compile_block(l->if_clause->else_clause, prog);
		    int jmp_dist = prog->head - end_of_then_clause;
		    memmove(then_clause_insert, &jmp_dist, 4);
		}
		break;
#ifdef OLD_IF_CODE
		char *f = NULL, *d = NULL; //labels for false, done
		if(l->if_clause->else_clause) {
		    need_else = 1;
		    f = get_new_label();
		}
		d = get_new_label();
		compile_expression(l->if_clause->test, text);
		write_inst_387("fldl (%esp)", text);
		write_inst_387("addl $8, %esp", text);  //add to pop off I think...
		write_inst_387("fldz", text);
		write_inst_387("fcomip", text);
		write_inst_387("ffree %st(0)", text);

		write_inst_SSE("pushl $0", text);
		write_inst_SSE("pushl $0", text);
		write_inst_SSE("movsd (%esp), %xmm0", text);
		write_inst_SSE("addl $8, %esp", text);
		write_inst_SSE("movsd (%esp), %xmm1", text);
		write_inst_SSE("ucomisd %xmm0, %xmm1", text);

		write_text("\tje ", text);
		if(need_else)
		    write_text(f, text);
		else
		    write_text(d, text);
		write_text("\n", text);
		compile_block(l->if_clause->then_clause, text);
		if(need_else) {
		    write_text("\tjmp ", text);
		    write_text(d, text);
		    write_text("\n", text);
		    write_text(f, text);
		    write_text(":\n", text);
		    compile_block(l->if_clause->else_clause, text);
		}
		write_text(d, text);
		write_text(":\n", text);
		free(d);
		free(f);
#endif /*OLD_IF_CODE*/
	    }
	    break;
	default:
	    assert(0);
	    break;
	}
	l = l->next;
    }
    return 0;
}

struct mc_buffer *compile (line *l)
{
    
#ifdef CALC7_COMPILE
    
    struct text_buffer *text  = malloc(sizeof(struct text_buffer));
    memset(text, 0, sizeof(struct text_buffer));
    write_text("\t.text\n\t.globl _program1\n", text);
    write_text("_program1:\n", text);
    write_inst("pushl %ebp", text);
    write_inst("movl %esp, %ebp", text);
    compile_loop(l, text);
    write_text("\tmovl $0, %eax\n\tleave\n\tret\n", text);
    return text;
    
#endif /*CALC7_COMPILE*/

    struct mc_buffer *buffy = malloc (sizeof(struct mc_buffer));
    memset (buffy, 0, sizeof(buffy));

    /* enter $0, $0 */
    push_hex_string("C8 00 00 00", buffy);
    compile_loop(l, buffy);
    /*load whatever is on the top of the stack into st(0) for return */
    /*fld (%esp)*/
    push_hex_string("DD 04 24", buffy);
    /*leave*/
    push_hex_string("C9", buffy);
    /*ret (near)*/    
    push_hex_string("C3", buffy);


    return buffy;

    /*********************************************************************
     *
     *  ok, cross fingers and set it up for execution.
     *
     *******************************************************************/

    void call_func(struct mc_buffer *prog);
    call_func(buffy);
    return NULL;
}
