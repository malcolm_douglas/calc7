#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "calc.h"

line *first_line = NULL;
FILE *input_file, *output_file;
symtable main_symtable;
value * main_valtable;
struct c_item *c_program;
extern struct function *funcs;

/*for now I am just going to edit this, but it wouldn't be too hard to have 
  both an interpretor and a byte code thingy. */

/*
enum {BLOCK, LINE, PROC, CALL, IF_CLAUSE, L_JUMP, L_LOOP, L_C_EXP};
 */

/* oh man, how do I handle built ins? #defines maybe */

int arg1;
int print_num;
int mod_part;
int mod_whole;
int mod;

void print_symbol (FILE *output, char *s, int align)
{
    fprintf(output_file,
	    "\t.globl _%s\n"
	    "\t.align %d\n"
	    "_%s:\n"
	    "\t.long %d\n",
	    s, align, s,
	    get_symbol_from_string(s, &main_symtable).id);
}

void install_function (void (*f_ptr) (void), symbol sym)
{
    main_valtable[sym.id].type = V_FUNC;
    main_valtable[sym.id].func = f_ptr;
}

void mod_f (void)
{
    double whole, part;
    part = modf(main_valtable[arg1].x, &whole);
    main_valtable[mod_whole].type = V_NUM;
    main_valtable[mod_whole].x = whole;
    main_valtable[mod_part].type = V_NUM;
    main_valtable[mod_part].x = part;    
}



void c_set_value (double x, symbol sym)
{
    main_valtable[sym.id].x = x;
}

void print_num_f (void)
{
    //    printf("in print_num.....\n");
    int arg1 = get_symbol_from_string("arg1", &main_symtable).id;
    printf("%f\n", main_valtable[arg1].x);
}

int main (int argc, char ** argv)
{
    struct mc_buffer *prog;
    struct text_buffer *text;
    void yyparse(void);
    if(argc == 3) {
	input_file = fopen(argv[1], "r");
	if(!input_file) {
	    printf("file not found -- \"%s\"\n", argv[1]);
	    exit(1);
	} 
	output_file = fopen(argv[2], "w");
	if(!output_file) {
	    printf("could not open %s for output.", argv[2]);
	    exit(1);
	}
    } else {
	printf("Usage: calc <in-file> <out-file>\n");
	exit(1);
    }
    init_symtable(&main_symtable);
    /*    declare_builtins(&main_symtable); */
    yyparse ();
    init_valtable(&main_symtable, &main_valtable); 	
    if(first_line) {
	arg1 = get_symbol_from_string("arg1", &main_symtable).id;
	mod = get_symbol_from_string("mod", &main_symtable).id;
	mod_part = get_symbol_from_string("mod_part", &main_symtable).id;
	mod_whole = get_symbol_from_string("mod_whole", &main_symtable).id;
	print_num = get_symbol_from_string("print_num", &main_symtable).id;

	main_valtable = malloc(main_symtable.length * sizeof(value));	
	c_set_value(20, get_symbol_from_string("arg1", &main_symtable));
	install_function(print_num_f, get_symbol_from_string("print_num", 
							   &main_symtable));
	install_function(mod_f, get_symbol_from_string("mod", &main_symtable));
	//	print_num();

	int i = get_symbol_from_string("mod_whole", &main_symtable).id;
	printf("mod_whole symbol id : %d\n", i);
	main_valtable[i].x = 5.00;

	prog = compile(first_line);

	clock_t c = clock();
	printf("Beginning program....\n");	
	call_func(prog);
	printf("took me %f seconds.\n",(float) (clock()-c)/ (float)(CLOCKS_PER_SEC));
	return 0;
	
	fprintf(output_file,
		"\n\t.comm _main_valtable, %d\n\n",
		main_symtable.length * sizeof(value));

	fprintf(output_file, "%s", text->buf);
#ifdef OLD_FUNCT_PRINT
	struct function *f = funcs;
	while(f) {
	    fprintf(output_file, "_%s:\n", f->name);
	    fprintf(output_file, "%s", f->body->buf);
	    f = f->next;
	}
#endif /*OLD_FUNCT_PRINT*/
    } else {
	printf("no worky\n");
    }
    return 0;
}

void init_valtable (symtable *st, value ** vt)
{
    *vt = malloc(sizeof(value) * st->length);
    assert(vt);
    st->valtable = *vt;
    memset(*vt, 0, sizeof(value) *st->length);
}

line *append_line(line *list, line *addme)
{
    line *tmp = list;
    while(tmp->next != NULL)
	tmp = tmp->next;
    tmp->next = addme;

    addme->next = NULL;
    return list;
}

expression *get_expression_from_num(double x)
{
    expression *ret = malloc(sizeof(expression));
    assert(ret);
    memset(ret, 0, sizeof(expression));
    ret->x = x;
    ret->type = NUM;
    return ret;
}
expression *get_expression_from_exp(int type, expression *a1, expression *a2)
{
    expression *ret = malloc(sizeof(expression));
    memset(ret, 0, sizeof(expression));
    ret->arg1  = a1;
    ret->arg2 = a2;
    ret->type = type;
    return ret;
}
expression *get_expression_from_sym(symbol s)
{
    expression *ret = malloc(sizeof(expression));
    memset(ret, 0, sizeof(expression));
    ret->type = VAR;
    ret->sym = s;
    return ret;
}
expression *get_assignment(symbol s, expression *e)
{
    expression *ret = malloc(sizeof(expression));
    memset(ret, 0, sizeof(expression));
    ret->type = ASS;
    ret->sym = s;
    ret->arg1 = e;
    return ret;
}
line *get_line (expression *exp)
{
    line *ret = malloc(sizeof(line));
    ret->exp = exp;
    ret->next = NULL;
    ret->type = LINE;
    return ret;
}
line *get_line_from_proc (proc *proc)
{
    line *ret = malloc(sizeof(line));
    ret->next = NULL;
    ret->proc = proc;
    ret->type = PROC;
    return ret;
}
line *get_line_from_call(symbol sym)
{
    line *ret = malloc(sizeof(line));
    memset(ret, 0, sizeof(line));
    ret->type = CALL;
    ret->sym = sym;
    return ret;
}
line *get_line_from_jump(symbol sym)
{
    line *ret = malloc(sizeof(line));
    memset(ret, 0, sizeof(line));
    ret->type = L_JUMP;
    ret->sym = sym;
    return ret;
}

line *get_line_from_if(if_clause *if_c)
{
    line *ret = malloc(sizeof(line));
    ret->next = NULL;
    ret->type = IF_CLAUSE;
    ret->if_clause = if_c;
    return ret;
}
line *get_line_from_loop(loop *l)
{
    line *ret = malloc(sizeof(line));
    ret->next = NULL;
    ret->type = L_LOOP;
    ret->loop = l;
    return ret;
}

line *get_block (line *l)
{
    line *ret = malloc(sizeof(line));
    ret->body = l;
    ret->next = NULL;
    ret->type = BLOCK;
    return ret;
}
proc *get_proc(symbol s, line *block) 
{
    proc *tmp = malloc(sizeof(proc));
    memset(tmp, 0, sizeof(proc));
    tmp->sym = s;
    tmp->block = block;
    return tmp;
}
if_clause *get_if_clause(expression *e, line *then_c, line *else_c)
{
    if_clause *ret = malloc(sizeof(if_clause));
    memset(ret, 0, sizeof(if_clause));
    ret->test = e;
    ret->then_clause = then_c;
    ret->else_clause = else_c;
    return ret;
}
loop *get_loop(expression *e, line *body)
{
    loop *ret = malloc(sizeof(loop));
    memset(ret, 0, sizeof(loop));
    ret->test = e;
    ret->body = body;
    return ret;
}
void init_symtable(symtable *st) 
{

    st->length = 0;
    st->total_size = 1024;
    st->table = malloc(sizeof(char *) * st->total_size);
    memset(st->table, 0, sizeof(char*) * st->total_size);
    st->valtable = NULL;
}
symbol get_symbol_from_string(char *str, symtable *st)
{
    int i;
    symbol s;
    s.st = st;
    for(i = 0; i < st->length; ++i) {
	if(!strcmp(str, st->table[i])) {
	    s.id = i;
	    return s;
	}
    }
    //ok, add it to list;
    if(st->length == st->total_size) {
	st->total_size *= 2;
	st->table = realloc(st->table, st->total_size * sizeof(char *));
	assert(st->table);
    }
    //not sure if blocks like this are bad style...
    //evertyhings agogo, add in the record and send it back.
    {
	int n = strlen(str);
	char *new_str = malloc(n + 1);
	strncpy(new_str, str, n+1);
	st->table[st->length] = new_str;
	s.id = st->length;
	st->length++;
    }
    return s;
}
char *get_string_from_symbol(symbol sym)
{
    return sym.st->table[sym.id];
}
double get_value(symbol s)
{
    return s.st->valtable[s.id].x;
}
value lookup_symbol(symbol s)
{
    return s.st->valtable[s.id];
}
/*
void set_value(symbol s, double x)
{
    value *v = s.st->valtable + s.id;
    v->x = x;
    v->type = V_NUM;
}

void set_proc(symbol s, int block)
{
    value *v = s.st->valtable + s.id;
    v->block = block;
    v->type = V_BLOCK;
}
void set_fun(symbol s, built_in bi)
{
    value *v = s.st->valtable + s.id;
    v->func  = bi;
    v->type = V_FUNC;
}
*/

